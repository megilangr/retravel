<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'MainController@index')->name('welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'Backend'], function () {
  Route::group(['middleware' => ['role:Administrator']], function () {
		Route::get('/', 'Backend\MainController@index')->name('admin.index');

		Route::resource('Users', 'Backend\UserController');
		Route::put('Users/{id}/Restore', 'Backend\UserController@restore')->name('Users.restore');
		Route::delete('Users/{id}/Permanent', 'Backend\UserController@permanentDelete')->name('Users.perma');
		
		Route::resource('Type', 'Backend\TypeController')->except(['create', 'show']);
		Route::put('Type/{id}/Restore', 'Backend\TypeController@restore')->name('Type.restore');
		Route::delete('Type/{id}/Permanent', 'Backend\TypeController@permanentDelete')->name('Type.perma');
		
		Route::resource('Currency', 'Backend\CurrencyController')->except(['create', 'show']);
		Route::put('Currency/{id}/Restore', 'Backend\CurrencyController@restore')->name('Currency.restore');
		Route::delete('Currency/{id}/Permanent', 'Backend\CurrencyController@permanentDelete')->name('Currency.perma');
		
		Route::resource('Package', 'Backend\PackageController');
		Route::put('Package/{id}/Restore', 'Backend\PackageController@restore')->name('Package.restore');
		Route::delete('Package/{id}/Permanent', 'Backend\PackageController@permanentDelete')->name('Package.perma');

		Route::resource('Itinerary', 'Backend\ItineraryController')->except(['index', 'create']);
		Route::get('/Itinerary/{id}/create', 'Backend\ItineraryController@index')->name('Itinerary.index');

	});
});