<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
						$table->bigIncrements('id');
						$table->string('name');
						$table->unsignedBigInteger('type_id');
						$table->foreign('type_id')->references('id')->on('types');
						$table->smallInteger('duration');
						$table->smallInteger('minimum');
						$table->unsignedBigInteger('currency_id');
						$table->foreign('currency_id')->references('id')->on('currencies');
						$table->date('expired_date');
						$table->text('description');
						$table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
