<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItinerariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itineraries', function (Blueprint $table) {
						$table->bigIncrements('id');
						$table->unsignedBigInteger('package_id');
						$table->foreign('package_id')->references('id')->on('packages');
						$table->smallInteger('day');
						$table->string('name');
						$table->string('transport')->nullable();
						$table->string('hotel')->nullable();
						$table->string('meals')->nullable();
						$table->text('description');
						$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itineraries');
    }
}
