@extends('backend.app')

@section('content')
<div class="row" style="margin-top: 15px;">
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-edit"></i>
					<span class="caption-subject bold"> &ensp; Form Data Jenis Tur</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						@if (isset($edit))
						<form action="{{ route('Type.update', $edit->id) }}" method="post" class="form">						
							@method('PUT')
						@else
						<form action="{{ route('Type.store') }}" method="post" class="form">
						@endif
							@csrf
							<div class="form-group {{ $errors->has('name') ? 'has-error':'' }}">
								<label class="bold">Nama Jenis Tur : <i class="text-danger">*</i></label>
								<input type="text" name="name" id="name" class="form-control flat" placeholder="Masukan Nama Jenis Tur..." value="{{ isset($edit) ? $edit->name:old('name') }}" autofocus="" style="margin-top: 6px;" required>
								<span class="help-block">
									{{ $errors->first('name') }}
								</span>
							</div>
							<hr style="margin-bottom: 12px;">
							<div class="form-group">
								@if (isset($edit))
								<button type="submit" class="btn green flat" style="margin: 3px;">
									<i class="fa fa-plus"></i> &ensp; Tambah Data
								</button>
								<button type="reset" class="btn btn-warning flat" style="margin: 3px;">
									<i class="fa fa-undo"></i> &ensp; Reset Input
								</button>
								<a href="{{ route('Type.index') }}" class="btn btn-danger flat" style="margin: 3px;">
									<i class="fa fa-times"></i> &ensp; Batal
								</a>
								@else
								<button type="submit" class="btn green flat" style="margin: 3px;">
									<i class="fa fa-plus"></i> &ensp; Tambah Data
								</button>
								<button type="reset" class="btn btn-danger flat" style="margin: 3px;">
									<i class="fa fa-undo"></i> &ensp; Reset Input
								</button>
								@endif
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-users"></i>
					<span class="caption-subject bold"> &ensp; Data Jenis Tur </span>
				</div>
				<div class="actions">
					<div class="btn-group">
						<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#trashed-modal">
							<i class="fa fa-trash"></i> &ensp; Tong Sampah 
						</button>
					</div>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<form action="{{ url('Backend/Users') }}" method="get" class="form-horizontal">
							<div class="form-group row">
								<label for="cari" class="col-xs-12 col-sm-2 col-md-4 col-lg-4 control-label text-left">Cari Data :</label>
								<div class="col-xs-12 col-sm-10 col-md-8 col-lg-8">
									@if (request()->has('cari'))
										<div class="input-group">
											<input type="text" name="cari" id="cari" class="form-control" placeholder="Cari Data Jenis Tur..." style="border-radius: 0px;" value="{{ request()->get('cari') ? request()->get('cari'):'' }}">
											<span class="input-group-addon" style="border-radius: 0px !important;">
												<a href="{{ route('Type.index') }}" style="border-radius: 0px; text-decoration: none !important;"><span class="fa fa-times"></span> Reset</a>
											</span>
										</div>
									@else 
										<input type="text" name="cari" id="cari" class="form-control" placeholder="Cari Data Jenis Tur..." style="border-radius: 0px;" value="{{ request()->get('cari') ? request()->get('cari'):'' }}">
									@endif
								</div>
							</div>
						</form>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th class="text-center">No.</th>
										<th>Nama Jenis</th>
										<th>Tanggal Buat</th>
										<th class="text-center">Aksi</th>
									</tr>
								</thead>
								<tbody>
									@php
										if (request()->has('page')) {
											$no = request()->get('page') * 10 - 10 + 1;
										} else {
											$no = 1;
										}
									@endphp
									@forelse ($type as $item)
										<tr>
											<td class="text-center" style="font-weight: 600; padding-top: 12px;">{{ $no++ }}.</td>
											<td style="padding-top: 12px;">{{ $item->name }} </td>
											<td style="padding-top: 12px;" width="18%">{{ date('d/m/Y H:i:s', strtotime($item->created_at)) }}</td>
											<td class="text-center" style="min-width: 100px !important;">
												<div class="btn-group">
													<a href="{{ route('Type.edit', $item->id) }}" class="btn btn-sm btn-warning flat">
														<i class="fa fa-edit"></i>
													</a>
													<a href="javascript:void(0)" class="btn btn-sm btn-danger" onclick="event.preventDefault(); document.getElementById('destroy'+{{ $item->id }}).submit();">
														<i class="fa fa-trash"></i>
													</a>
													<form action="{{ route('Type.destroy', $item->id) }}" method="post" id="destroy{{ $item->id }}">
														@csrf
														@method('DELETE')
													</form>
												</div>
											</td>
										</tr>
									@empty
										<tr>
											<td colspan="4" class="text-center">Belum Ada Data Jenis Tur.</td>
										</tr>
									@endforelse
								</tbody>
								<tfoot>
									<tr>
										<td colspan="6" class="text-right">
											{{ $type->links() }}
										</td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="trashed-modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Tempat Sampah Data Jenis Tur</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive" style="max-height: 500px;">
					<table class="table">
						<thead>
							<tr>
								<th class="text-center">No.</th>
								<th>Nama Jenis Tur</th>
								<th>Tanggal Hapus</th>
								<th class="text-center">Aksi</th>
							</tr>
						</thead>
						<tbody>
							@forelse ($ttype as $item)
								<tr>
									<td class="text-center" style="font-weight: 600; padding-top: 12px;">{{ $loop->iteration }}.</td>
									<td style="padding-top: 12px;">{{ $item->name }} </td>
									<td style="padding-top: 12px;" width="18%">{{ date('d/m/Y H:i:s', strtotime($item->deleted_at)) }}</td>
									<td class="text-center" style="min-width: 100px !important;">
										<div class="btn-group">
											<a href="javascript:void(0)" class="btn btn-sm btn-success flat" onclick="event.preventDefault(); document.getElementById('restore'+{{ $item->id }}).submit();">
												<i class="fa fa-undo"></i> Pulihkan
											</a>
											<a href="javascript:void(0)" class="btn btn-sm btn-danger" onclick="event.preventDefault(); document.getElementById('perma'+{{ $item->id }}).submit();">
												<i class="fa fa-trash"></i> Hapus
											</a>
											
											<form action="{{ route('Type.restore', $item->id) }}" method="post" id="restore{{ $item->id }}">
												@csrf
												@method('PUT')
											</form>
											<form action="{{ route('Type.perma', $item->id) }}" method="post" id="perma{{ $item->id }}">
												@csrf
												@method('DELETE')
											</form>
										</div>
									</td>
								</tr>
							@empty
								
							@endforelse
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer justify-content-between">
				<button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-outline-light">Save changes</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

@endsection