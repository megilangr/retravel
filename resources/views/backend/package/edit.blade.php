@extends('backend.app')

@section('css')
<link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />

<style>
	.flat {
		border-radius: 0px !important;
	}
	.select2-container--bootstrap .select2-selection {
		border-radius: 0px !important;
	}
</style>
@endsection

@section('content')
<div class="row" style="margin-top: 15px;">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-plane"></i>
					<span class="caption-subject bold"></span> &ensp; Form Tambah Paket Tur
				</div>
				<div class="actions">
					<div class="btn-group">
						<a href="{{ route('Package.index') }}" class="btn red">
							<i class="fa fa-times"></i> &ensp; Kembali
						</a>
					</div>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<form action="{{ route('Package.update', $edit->id) }}" method="post" class="form">
							@csrf
							@method('PUT')	
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
									<div class="form-group {{ $errors->has('name') ? 'has-error':'' }}">
										<label class="bold">Nama Paket Tur : <i class="text-danger">*</i></label>
										<input type="text" name="name" id="name" class="form-control flat" placeholder="Masukan Nama Paket Tur..." value="{{ $edit->name }}" autofocus="" required>
										<span class="help-block">
											{{ $errors->first('name') }}
										</span>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
									<div class="form-group {{ $errors->has('type_id') ? 'has-error':'' }}">
										<label class="bold">Tipe Jenis Tur : <i class="text-danger">*</i></label>
										<select name="type_id" id="type_id" class="form-control flat select2" data-placeholder="Pilih Tipe Jenis Tur..." style="width: 100% !important;">
											<option value=""></option>
											@foreach ($type as $item)
												<option value="{{ $item->id }}" {{ $edit->type_id == $item->id ? 'selected':'' }}>{{ $item->name }}</option>
											@endforeach
										</select>
										<span class="help-block">
											{{ $errors->first('type_id') }}
										</span>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
									<div class="form-group {{ $errors->has('duration') ? 'has-error':'' }}">
										<label class="bold">Durasi Tur : <i class="text-danger">*</i></label>
										<div class="input-group">
											<input type="text" name="duration" id="duration" class="form-control flat" min="1" placeholder="1..." value="{{ $edit->duration }}" autofocus="" required>
											<span class="input-group-addon flat"> Hari</span>
										</div>
										<span class="help-block">
											{{ $errors->first('duration') }}
										</span>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
									<div class="form-group {{ $errors->has('minimum') ? 'has-error':'' }}">
										<label class="bold">Minimum Peserta : <i class="text-danger">*</i></label>
										<div class="input-group">
											<input type="number" name="minimum" id="minimum" class="form-control flat" min="1" placeholder="1..." value="{{ $edit->minimum }}" autofocus="" required>
											<span class="input-group-addon flat"> Orang</span>
										</div>
										<span class="help-block">
											{{ $errors->first('minimum') }}
										</span>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<div class="form-group {{ $errors->has('currency_id') ? 'has-error':'' }}">
										<label class="bold">Mata Uang Pembayaran : <i class="text-danger">*</i></label>
										<select name="currency_id" id="currency_id" class="form-control flat select2" data-placeholder="Pilih Mata Uang Tur..." style="width: 100% !important;">
											<option value=""></option>
											@foreach ($currency as $item)
												<option value="{{ $item->id }}" {{ $edit->currency_id == $item->id ? 'selected':'' }}>{{ $item->code }} - {{ $item->name }}</option>
											@endforeach
										</select>
										<span class="help-block">
											{{ $errors->first('currency_id') }}
										</span>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
									<div class="form-group {{ $errors->has('expired_date') ? 'has-error':'' }}">
										<label class="bold">Ketersediaan : <i class="text-danger">*</i></label>
										<div class="input-group">
										<span class="input-group-addon flat"><i class="fa fa-calendar" style="color: #515151;"></i></span>
											<input type="text" name="expired_date" id="expired_date" class="form-control datepicker flat" placeholder="Hari/Bulan/Tahun" value="{{ date('d/m/Y', strtotime($edit->expired_date)) }}" style="background-color: #ffffff;" readonly required>
										</div>
										<span class="help-block">
											{{ $errors->first('expired_date') }}
										</span>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="form-group {{ $errors->has('description') ? 'has-error':'' }}">
										<label for="bold">Deskripsi Tur : <i class="text-danger">*</i></label>
										<textarea name="description" id="description" rows="3" class="form-control flat" required>{{ $edit->description }}</textarea>
										<span class="help-block">
											{{ $errors->first('description') }}
										</span>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
									<button type="submit" class="btn btn-success btn-block flat" name="detail">
										<i class="fa fa-check"></i> &ensp;
										Ubah Paket Tur
									</button>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
									<button type="reset" class="btn btn-danger btn-block flat">
										<i class="fa fa-undo"></i> &ensp;
										Reset Input
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('script')
<script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/other/ckeditor/ckeditor.js') }}" type="text/javascript"></script>

<script>
	$(document).ready(function() {
		$('.select2').select2();
		$('.datepicker').datepicker({
			format: 'dd/mm/yyyy',
			autoclose: true,
			startDate: "+1d"
		});

		CKEDITOR.replace('description');
	});
</script>
@endsection