@extends('backend.app')	

@section('css')
<style>
	.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
    color: #575757;
    background-color: #52f4ff99;
    border: 1px solid #ddd;
    /* border-bottom-color: transparent; */
    /* border-top-color: transparent; */
    cursor: default;
		border-radius: 0px;
	}

	.myF {
		line-height: 20px; 
		font-weight: 500;
		margin-bottom: 20px;
	}
</style>
@endsection

@section('content')
<div class="row" style="margin-top: 15px;">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="portlet light bordered">
			<div class="portlet-title" style="margin-bottom: 0px;">
				<div class="caption">
					<i class="fa fa-plane"></i>
					<span class="caption-subject bold"> &ensp; Detail Paket Tur </span>
				</div>
				<div class="actions">
					<div class="btn-group">
						<a href="{{ route('Package.index') }}" class="btn btn-danger">
							<i class="fa fa-times"></i> &ensp; Kembali
						</a>
					</div>
				</div>
			</div>
			<div class="portlet-body" style="padding-top: 0px;">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist" style="margin-bottom: 0px; border-right: 1px solid #ddd; border-left: 1px solid #ddd;">
							<li class="nav-item active">
								<a class="nav-link flat" id="detail-tab" data-toggle="pill" href="#detail" role="tab" aria-controls="detail" aria-selected="true">Detail</a>
							</li>
							<li class="nav-item">
								<a class="nav-link flat" id="custom-tabs-three-profile-tab" data-toggle="pill" href="#custom-tabs-three-profile" role="tab" aria-controls="custom-tabs-three-profile" aria-selected="false">Rencana Perjalanan</a>
							</li>
							<li class="nav-item">
								<a class="nav-link flat" id="custom-tabs-three-messages-tab" data-toggle="pill" href="#custom-tabs-three-messages" role="tab" aria-controls="custom-tabs-three-messages" aria-selected="false">Messages</a>
							</li>
							<li class="nav-item">
								<a class="nav-link flat" id="custom-tabs-three-settings-tab" data-toggle="pill" href="#custom-tabs-three-settings" role="tab" aria-controls="custom-tabs-three-settings" aria-selected="false">Settings</a>
							</li>
						</ul>
						<div class="tab-content" id="custom-tabs-three-tabContent" style="border: 1px solid #ddd; border-top-color: transparent; padding: 10px;">
							<div class="tab-pane active" id="detail" role="tabpanel" aria-labelledby="detail-tab" style="padding: 10px;">
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<h4>
											<i class="fa fa-info"></i> &ensp;
											<b>Detail Paket Tur</b>
											<a href="{{ route('Package.edit', $package->id) }}" class="btn btn-sm btn-warning pull-right"> 
												<i class="fa fa-pencil"></i> &ensp;
												Ubah Data
											</a>
										</h4>
										<hr style="margin-top: 20px; margin-bottom: 10px;">
										<div class="row" style="padding: 5px;">

											<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3"><h5><b>Nama Paket Tur <span class="hidden-md hidden-lg">&ensp; : </span></b></h5></div>
											<div class="hidden-xs hidden-sm col-md-1 col-lg-1"><h5><b>:</b></h5></div>
											<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8"><h5>{{ $package->name }}</h5></div>

											<div class="col-xs-11 col-sm-11 col-md-3 col-lg-3"><h5><b>Jenis Tur <span class="hidden-md hidden-lg">&ensp; : </span></b></h5></div>
											<div class="hidden-xs hidden-sm col-md-1 col-lg-1"><h5><b>:</b></h5></div>
											<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8"><h5>{{ $package->type->name }}</h5></div>

											<div class="col-xs-11 col-sm-11 col-md-3 col-lg-3"><h5><b>Durasi Tur <span class="hidden-md hidden-lg">&ensp; : </span></b></h5></div>
											<div class="hidden-xs hidden-sm col-md-1 col-lg-1"><h5><b>:</b></h5></div>
											<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8"><h5>{{ $package->duration }} Hari</b></h5></div>

											<div class="col-xs-11 col-sm-11 col-md-3 col-lg-3"><h5><b>Minimal Peserta <span class="hidden-md hidden-lg">&ensp; : </span></b></h5></div>
											<div class="hidden-xs hidden-sm col-md-1 col-lg-1"><h5><b>:</b></h5></div>
											<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8"><h5>{{ $package->minimum }} Orang</b></h5></div>
											
											<div class="col-xs-11 col-sm-11 col-md-3 col-lg-3"><h5><b>Mata Uang Pembayaran <span class="hidden-md hidden-lg">&ensp; : </span></b></h5></div>
											<div class="hidden-xs hidden-sm col-md-1 col-lg-1"><h5><b>:</b></h5></div>
											<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8"><h5>{{ $package->currency->code }} - {{ $package->currency->name }}</h5></div>

											<div class="col-xs-11 col-sm-11 col-md-3 col-lg-3"><h5><b>Ketersediaan Tur <span class="hidden-md hidden-lg">&ensp; : </span></b></h5></div>
											<div class="hidden-xs hidden-sm col-md-1 col-lg-1"><h5><b>:</b></h5></div>
											<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8"><h5>{{ date('d / M / Y', strtotime($package->expired_date)) }}</h5></div>
											
											<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4"><h5><b>Deskripsi &ensp; :</b></h5></div>
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 5px;">
												{!! $package->description !!}
											</div>
											
											
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="custom-tabs-three-profile" role="tabpanel" aria-labelledby="custom-tabs-three-profile-tab" style="padding: 10px;">
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<h4>
											<i class="fa fa-bus"></i> &ensp;
											<b>Rencana Perjalanan</b>
											<a href="{{ route('Itinerary.index', $package->id) }}" class="btn btn-sm btn-warning pull-right"> 
												<i class="fa fa-pencil"></i> &ensp;
												Atur Rencana
											</a>
										</h4>
										<hr style="margin-top: 20px; margin-bottom: 10px;">
										<div class="row">
											<div class="col-md-3 col-sm-3 col-xs-3">
												<ul class="nav nav-tabs tabs-left">
													@for ($i = 1; $i <= $package->duration; $i++)
													<li class="{{ $i == 1 ? 'active':'' }}">
														<a href="#tab_6_{{ $i }}" data-toggle="tab" aria-expanded="true"> Hari Ke-{{ $i }} </a>
													</li>
													@endfor
												</ul>
											</div>
											<div class="col-md-9 col-sm-9 col-xs-9">
												<div class="tab-content">
													@for ($i = 1; $i <= $package->duration; $i++)
													@php
														$ada = false;
														$name = '';
														$transport = '';
														$hotel = '';
														$meals = '';
														$desc = '';
														foreach ($package->itinerary as $item) {
															if ($item->day == $i) {
																$ada = true;
																$name = $item->name;
																$transport = $item->transport;
																$hotel = $item->hotel;
																$meals = $item->meals;
																$desc = $item->description;
															}
														}
													@endphp
													<div class="tab-pane {{ $i == 1 ? 'active':'' }} in" id="tab_6_{{ $i }}">
														@if ($ada)
															<h4 class="bold">{{ $name }}</h4>
															<hr>
															<h5 class="myF">{{ $desc }}</h5>
															@if ($hotel != '')
															<h5 class="myF">Hotel : {{ $hotel }}</h5>
															@endif
															@if ($meals != '')
																@php
																	$x = explode(',', $meals);
																@endphp
																@foreach ($x as $item)
																	<button class="btn btn-sm btn-secondary" style="margin: 4px;"> <span class="fa fa-cutlery"></span> &ensp; {{ $item }}</button>
																@endforeach
															@endif
														@else
															Belum ada rencana perjalanan pada hari ke-{{$i}}
														@endif
													</div>
													@endfor
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="custom-tabs-three-messages" role="tabpanel" aria-labelledby="custom-tabs-three-messages-tab" style="padding: 10px;">
								Null
							</div>
							<div class="tab-pane fade" id="custom-tabs-three-settings" role="tabpanel" aria-labelledby="custom-tabs-three-settings-tab" style="padding: 10px;">
								Null
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection