<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Padma Tours Backend</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Scripts -->
    {{-- <script src="{{ asset('js/app.js') }}"></script> --}}
    <script src="{{ asset('assets/global/plugins/pace/pace.min.js') }}" type="text/javascript"></script>
    <!-- Fonts -->
    <link href="{{ asset('css/opensans.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Styles -->
    <link href="{{ asset('assets/global/plugins/pace/themes/pace-theme-flash.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/css/components-rounded.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/layouts/layout/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/layouts/layout/css/themes/darkblue.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="{{ asset('favicon.png') }}" />

		<link rel="stylesheet" href="{{ asset('other/toastr/toastr.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/other/lightbox2/css/lightbox.css') }}">

		<style>
			.flat {
				border-radius: 0px !important;	
			}
		</style>

    @yield('css')
    
</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
    <div id="app">
        @include('backend.layouts.navbar')
        <div class="clearfix"> </div>
        <div class="page-container">
						@include('backend.layouts.sidebar')
						
            <div class="container-fluid" style="padding-left: 0px !important; padding-right: 0px !important;">
            {{-- <div class="container-fluid"> --}}
                <div class="page-content-wrapper">
                    <div class="page-content">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
        <div class="page-footer pull-right">
            <div class="page-footer-inner">
                2019 &copy; Padma Tours | v1.0
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
    </div>
    <!-- Scripts -->
    <script src="{{ asset('assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/scripts/app.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/layouts/layout/scripts/layout.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript"></script>

		<script src="{{ asset('other/toastr/toastr.min.js') }}"></script>
		<script src="{{ asset('assets/other/lightbox2/js/lightbox.js') }}"></script>

		@if (session('success'))
			<script>
				toastr.success("{{ session('success') }}", 'Berhasil !');
			</script>
		@endif

		@if (session('info'))
			<script>
				toastr.info("{{ session('info') }}", 'Informasi !');
			</script>
		@endif

		@if (session('warning'))
			<script>
				toastr.warning("{{ session('warning') }}", 'Peringatan !');
			</script>
		@endif

		@if (session('error'))
			<script>
				toastr.error("{{ session('error') }}", 'Kesalahan !');
			</script>
		@endif

		<script>
			function toastSuccess(message) {
				toastr.success(message, "Berhasil !");
			}
			function toastInfo(message) {
				toastr.info(message, "Informasi !");
			}
			function toastWarning(message) {
				toastr.warning(message, "Peringatan !");
			}
			function toastError(message) {
				toastr.error(message, "Kesalahan !");
			}
		</script>

		

    @yield('script')
    {{-- <script src="{{ asset('assets/global/plugins/bootstrap-growl/jquery.bootstrap-growl.min.js') }}" type="text/javascript"></script> --}}
    {{-- <script src="{{ asset('assets/global/plugins/amcharts/amcharts/amcharts.js') }}" type="text/javascript"></script> --}}
    {{-- <script src="{{ asset('assets/global/plugins/amcharts/amcharts/serial.js') }}" type="text/javascript"></script> --}}
    {{-- <script src="https://js.pusher.com/5.0/pusher.min.js"></script> --}}
    {{-- <script src="{{ asset('assets/moment/moment.min.js') }}"></script> --}}
</body>
</html>
