<div class="page-header navbar navbar-fixed-top">
	<div class="page-header-inner ">
			<div class="page-logo">
					<a href="{{ url('/') }}">
							<img src="{{ asset('logo-backend.png') }}" alt="Logo Aplikasi" class="logo-default" style="height: 27px; margin-top: 10px" />
					</a>
					<div class="menu-toggler sidebar-toggler">
							<span></span>
					</div>
			</div>
			<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
					<span></span>
			</a>
			<div class="top-menu">
					<ul class="nav navbar-nav pull-right">
							<li class="dropdown dropdown-extended dropdown-inbox">
									<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
											<i class="icon-bell"></i>
											<span class="badge badge-danger">
											<div id="notif">0</div>
											</span>
									</a>
									<ul class="dropdown-menu">
											<li class="external">
													<h3>
													<span class="bold" id="count">0</span> pemberitahuan baru 
													</h3>
											</li>
											{{-- <li>
													<ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
															@forelse (auth()->user()->notifications as $item)
																@foreach ($item->data as $item2)
																@php 
																	$datetime1 = new DateTime();
																	$datetime2 = new DateTime($item->created_at);

																	$interval = $datetime1->diff($datetime2);

																	$tahun = $interval->format('%y');
																	$bulan = $interval->format('%m');
																	$hari = $interval->format('%a');
																	$jam = $interval->format('%h');
																	$menit = $interval->format('%i');
																	$detik = $interval->format('%s');

																	// echo "<script>console.log('$item->notifiable_id : $tahun Tahun $bulan Bulan $hari Hari $jam Jam $menit Menit $detik Detik')</script>";
																
																	if ($tahun != '0') {
																		$waktu = $tahun . ' Tahun Lalu';
																	} else if ($bulan != '0') {
																		$waktu = $bulan . ' Bulan Lalu';
																	} else if ($hari != '0') {
																		$waktu = $hari . ' Hari Lalu';
																	} else if ($jam != '0') {
																		$waktu = $jam . ' Jam Lalu';
																	} else if ($menit != '0') {
																		$waktu = $menit . ' Menit Lalu';
																	} else if ($detik != '0') {
																		$waktu = 'Baru Saja';
																	} else {
																		$waktu = 'Baru Saja';
																	}
																@endphp
																<li>
																		<a href="{{ url('backend') }}/{{ $item2['url'] }}">
																				<span class="photo"><img src="{{ asset('assets/layouts/layout/img/avatar.png') }}" class="img-circle" alt=""> </span>
																				<span class="subject">
																						<span class="from">{{ $item2['first_name'] }}</span>
																						<span class="time">{{ $waktu }}</span>
																				</span>
																				<span class="message">{{ $item2['message'] }}</span>
																		</a>
																</li>
																@endforeach
															@empty
															<li>
																	<a href="#">
																			<span class="message">Belum Ada Notifikasi Pemberi Tahuan</span>
																	</a>
															</li>
															@endforelse
													</ul>
											</li> --}}
											<li class="external">
													<h3 class="text-center">
															<a href="{{ url('backend/notifications') }}">
															<u>Lihat Semua Pemberitahuan</u>
															</a>
													</h3>
											</li>
									</ul>
							</li>
							<li class="dropdown dropdown-user">
									<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
											<img alt="" class="img-circle" src="{{ asset('assets/layouts/layout/img/avatar.png') }}" />
											<span class="username username-hide-on-mobile"> {{ Auth::user()->first_name }} </span>
											<i class="fa fa-angle-down"></i>
									</a>
									<ul class="dropdown-menu dropdown-menu-default">
											<li>
													<a href="{{ url('backend/users/changepassword') }}">
															<i class="fa fa-key"></i> Ganti Kata Sandi
													</a>
											</li>
											<li>
													<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
															<i class="fa fa-sign-out"></i> Keluar
													</a>
													<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
															@csrf
													</form>
											</li>
									</ul>
							</li>
					</ul>
			</div>
	</div>
</div>