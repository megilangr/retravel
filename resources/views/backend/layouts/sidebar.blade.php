<div class="page-sidebar-wrapper">
	<div class="page-sidebar navbar-collapse collapse">
			<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 2px;">
					<li class="sidebar-toggler-wrapper hide">
							<div class="sidebar-toggler">
									<span></span>
							</div>
					</li>
					<li class="heading">
							<h3 class="uppercase">Main Menu</h3>
					</li>
					<li class="nav-item start {{ Request::is('backend') ? 'active' : '' }}">
							<a href="{{ url('/backend') }}" class="nav-link nav-toggle">
									<i class="icon-bar-chart"></i>
									<span class="title">Dashboard</span>
									<span class="selected"></span>
							</a>
					</li>
					<li class="nav-item start {{ Request::is(['Backend/Type*', 'Backend/Currency*']) ? 'active' : '' }}">
							<a href="javascript:;" class="nav-link nav-toggle">
									<i class="fa fa-edit"></i>
									<span class="title">Master Data</span>
									<span class="selected"></span>
									<span class="arrow {{ Request::is(['Backend/Type*']) ? 'open' : '' }}"></span>
							</a>
							<ul class="sub-menu">
									<li class="nav-item {{ Request::is('Backend/Type*') ? 'active' : '' }}">
											<a href="{{ route('Type.index') }}" class="nav-link ">
													<span class="title">Jenis Tur</span>
											</a>
									</li>
									<li class="nav-item {{ Request::is('Backend/Currency*') ? 'active' : '' }}">
											<a href="{{ route('Currency.index') }}" class="nav-link ">
													<span class="title">Mata Uang</span>
											</a>
									</li>
							</ul>
					</li>
					<li class="nav-item start {{ Request::is(['backend/packages*', 'backend/customers*', 'backend/hotoffers*', 'backend/payments*', 'backend/preorder*']) ? 'active' : '' }}">
							<a href="javascript:;" class="nav-link nav-toggle">
									<i class="icon-notebook"></i>
									<span class="title">Manajemen</span>
									<span class="selected"></span>
									<span class="arrow {{ Request::is(['Backend/Package*', 'backend/customers*', 'backend/hotoffers*', 'backend/payments*', 'backend/preorder*', 'backend/bookings*']) ? 'open' : '' }}"></span>
							</a>
							<ul class="sub-menu">
									<li class="nav-item {{ Request::is('backend/packages*') ? 'active' : '' }}">
											<a href="{{ route('Package.index') }}" class="nav-link ">
													<span class="title">Paket Tour</span>
											</a>
									</li>
									<li class="nav-item {{ Request::is('backend/hotoffers*') ? 'active' : '' }}">
											<a href="{{ url('backend/hotoffers') }}" class="nav-link ">
													<span class="title">Promo</span>
											</a>
									</li>
									<li class="nav-item {{ Request::is('backend/customers*') ? 'active' : '' }}">
											<a href="{{ url('backend/customers') }}" class="nav-link ">
													<span class="title">Customer</span>
											</a>
									</li>
									<li class="nav-item {{ Request::is('backend/bookings*') ? 'active' : '' }}">
											<a href="{{ url('backend/booking') }}" class="nav-link ">
													<span class="title">Pemesanan</span>
											</a>
									</li>
									<li class="nav-item {{ Request::is('backend/payments*') ? 'active' : '' }}">
											<a href="{{ url('backend/payments') }}" class="nav-link ">
													<span class="title">Pembayaran</span>
											</a>
									</li>
									<li class="nav-item {{ Request::is('backend/preorder*') ? 'active' : '' }}">
											<a href="{{ url('backend/preorders') }}" class="nav-link ">
													<span class="title">Custom Tour</span>
											</a>
									</li>
							</ul>
					</li>
					<li class="nav-item start {{ Request::is(['Backend/Users*']) ? 'active' : '' }}">
							<a href="javascript:;" class="nav-link nav-toggle">
									<i class="icon-settings"></i>
									<span class="title">Pengaturan</span>
									<span class="selected"></span>
									<span class="arrow {{ Request::is(['Backend/Users*']) ? 'open' : '' }}"></span>
							</a>
							<ul class="sub-menu">
									<li class="nav-item {{ Request::is('Backend/Users*') ? 'active' : '' }}">
											<a href="{{ route('Users.index') }}" class="nav-link ">
													<span class="title">Pengguna</span>
											</a>
									</li>
									<li class="nav-item {{ Request::is('backend/permissions*') ? 'active' : '' }}">
											<a href="{{ url('backend/permissions') }}" class="nav-link ">
													<span class="title">Hak Akses</span>
											</a>
									</li>
							</ul>
					</li>
			</ul>
	</div>
</div>