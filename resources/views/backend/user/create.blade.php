@extends('backend.app')

@section('css')

<link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />

<style>
	.flat {
		border-radius: 0px !important;
	}
	.select2-container--bootstrap .select2-selection {
		border-radius: 0px !important;
	}
</style>

@endsection

@section('content')
<div class="row" style="margin-top: 15px;">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-user"></i>
					<span class="caption-subject bold"></span> &ensp; Form Tambah Baru Pengguna
				</div>
				<div class="actions">
					<a href="{{ route('Users.index') }}" class="btn red">
						<i class="fa fa-times"></i> &ensp; Kembali
					</a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<form action="{{ route('Users.store') }}" method="post" class="form" enctype="multipart/form-data">
							@csrf
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<div class="form-group {{ $errors->has('first_name') ? 'has-error':'' }}">
										<label class="bold">Nama Depan : <i class="text-danger">*</i></label>
										<input type="text" name="first_name" id="first_name" class="form-control flat" placeholder="Masukan Nama Depan..." value="{{ old('first_name') }}" autofocus="" required>
										<span class="help-block">
											{{ $errors->first('first_name') }}
										</span>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<div class="form-group {{ $errors->has('last_name') ? 'has-error':'' }}">
										<label class="bold">Nama Belakang : <i class="text-danger">*</i></label>
										<input type="text" name="last_name" id="last_name" class="form-control flat" placeholder="Masukan Nama Belakang..." value="{{ old('last_name') }}" required>
										<span class="help-block">
											{{ $errors->first('last_name') }}
										</span>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<div class="form-group {{ $errors->has('last_name') ? 'has-error':'' }}">
										<label class="bold">Negara Asal : <i class="text-danger">*</i></label>
										<select name="country_id" id="country_id" class="form-control select2 flat" data-placeholder="Pilih Negara Asal..." style="width: 100%;" required>	
											<option value=""></option>
											@foreach ($country as $item)
												<option value="{{ $item->id }}" {{ old('country_id') == $item->id ? 'selected':'' }}>{{ $item->code }} - {{ $item->name }}</option>
											@endforeach
										</select>
										<span class="help-block">
											{{ $errors->first('country_id') }}
										</span>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<div class="form-group {{ $errors->has('date_of_birth') ? 'has-error':'' }}">
										<label class="bold">Tanggal Lahir : <i class="text-danger">*</i></label>
										<input type="text" name="date_of_birth" id="date_of_birth" class="form-control datepicker flat" placeholder="Hari/Bulan/Tahun" value="{{ old('date_of_birth') ? old('date_of_birth'):'01/01/1990' }}" style="background-color: #ffffff;" readonly required>
										<span class="help-block">
											{{ $errors->first('date_of_birth') }}
										</span>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<div class="form-group {{ $errors->has('gender') ? 'has-error':'' }}">
										<label for="" class="bold">Jenis Kelamin : <i class="text-danger">*</i></label>
										<select name="gender" id="gender" class="form-control flat" required>
											<option value="">Pilih Jenis Kelamin</option>
											<option value="1" {{ old('gender') == 1 ? 'selected':'' }}>Laki-Laki</option>
											<option value="2" {{ old('gender') == 2 ? 'selected':'' }}>Perempuan</option>
											<option value="3" {{ old('gender') == 3 ? 'selected':'' }}>Lainnya</option>
										</select>
									</div>
									<span class="help-block">
										{{ $errors->first('gender') }}
									</span>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<div class="form-group {{ $errors->has('phone') ? 'has-error':'' }}">
										<label for="" class="bold">Nomor Telfon : <i class="text-danger">*</i></label>
										<div class="input-group">
											<span class="input-group-addon">
												( +62 )
											</span>
											<input type="text" name="phone" id="phone" class="form-control flat" placeholder="Masukan Nomor Telfon..." value="{{ old('phone') }}" required>
										</div>
										<span class="help-block">
											{{ $errors->first('phone') }}
										</span>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="form-group {{ $errors->has('address') ? 'has-error':'' }}">
										<label for="" class="bold">Alamat : <i class="text-danger">*</i></label>
										<textarea name="address" id="address" class="form-control flat" placeholder="Masukan Alamat..." required>{{ old('address') }}</textarea>
										<span class="help-block">
											{{ $errors->first('phone') }}
										</span>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div class="form-group {{ $errors->has('email') ? 'has-error':'' }}">
										<label for="" class="bold">E-Mail : <i class="text-danger">*</i></label>
										<input type="email" name="email" id="email" class="form-control flat" placeholder="Masukan E-Mail..." required value="{{ old('email') }}">
										<span class="help-block">	
											{{ $errors->first('email') }}
										</span>
									</div>
									<div class="form-group {{ $errors->has('email_confirmation') ? 'has-error':'' }}">
										<label for="" class="bold">Konfirmasi E-Mail : <i class="text-danger">*</i></label>
										<input type="email" name="email_confirmation" id="email_confirmation" class="form-control flat" placeholder="Tulis Ulang E-Mail..." required>
										<span class="help-block">
											{{ $errors->first('email_confirmation') }}
										</span>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div class="form-group {{ $errors->has('password') ? 'has-error':'' }}">
										<label for="" class="bold">Password : <i class="text-danger">*</i></label>
										<input type="password" name="password" id="password" class="form-control flat" placeholder="Masukan Password..." required>
										<span class="help-block">
											{{ $errors->first('password') }}
										</span>
									</div>
									<div class="form-group {{ $errors->has('password_confirmation') ? 'has-error':'' }}">
										<label for="" class="bold">Konfirmasi Password : <i class="text-danger">*</i></label>
										<input type="password" name="password_confirmation" id="password_confirmation" class="form-control flat" placeholder="Tulis Ulang Password..." required>
										<span class="help-block">
											{{ $errors->first('password_confirmation') }}
										</span>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="form-group {{ $errors->has('photo') ? 'has-error':'' }}">
										<label for="" class="bold">Photo : </label>
										<input type="file" name="photo" id="photo" class="form-control flat" placeholder="Upload Foto...">
										<span class="help-block" style="color: #868686 !important;">
											<i>
												* Photo Harus Memiliki Format : JPG, JPEG, PNG. 
											</i>
										</span>
										<span class="help-block">
											{{ $errors->first('photo') }}
										</span>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="form-group {{ $errors->has('roles') ? 'has-error':'' }}">
										<label for="" class="bold">Hak Akses : </label>
										@foreach ($role as $item)
											<div class="radio">
												<label for="roles{{ $loop->iteration }}">
													<input type="radio" name="roles" id="roles{{ $loop->iteration }}" value="{{ $item->name }}" {{ old('roles') == $item->name ? 'checked':'' }} required>
													{{ $item->name }}
												</label>
											</div>
										@endforeach
										<span class="help-block">
											{{ $errors->first('roles') }}
										</span>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<hr>
									<button type="submit" class="btn green flat">
										<i class="fa fa-plus"></i> &ensp; Tambah Data
									</button>
									<button type="reset" class="btn red flat">
										<i class="fa fa-undo"></i> &ensp; Reset Input
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
<script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>

<script>
	$(document).ready(function() {
		$('.select2').select2();
		$('.datepicker').datepicker({
			format: 'dd/mm/yyyy',
			autoclose: true,
			orientation: "bottom",
		});
	});
</script>
@endsection