@extends('backend.app')

@section('content')
<div class="row" style="margin-top: 15px;">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-users"></i>
					<span class="caption-subject bold"> &ensp; Data Pengguna </span>
				</div>
				<div class="actions">
					<div class="btn-group">
						<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#trashed-modal">
							<i class="fa fa-trash"></i> 
						</button>

						<a href="{{ route('Users.create') }}" class="btn blue">
							<i class="icon-pencil"></i> &ensp; Tambah Baru
						</a>
					</div>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<form action="{{ url('Backend/Users') }}" method="get" class="form-horizontal">
							<div class="form-group row">
								<label for="cari" class="col-xs-12 col-sm-2 col-md-4 col-lg-4 control-label text-left">Cari Data :</label>
								<div class="col-xs-12 col-sm-10 col-md-8 col-lg-8">
									@if (request()->has('cari'))
										<div class="input-group">
											<input type="text" name="cari" id="cari" class="form-control" placeholder="Cari Data Pengguna..." style="border-radius: 0px;" value="{{ request()->get('cari') ? request()->get('cari'):'' }}">
											<span class="input-group-addon" style="border-radius: 0px !important;">
												<a href="{{ route('Users.index') }}" style="border-radius: 0px; text-decoration: none !important;"><span class="fa fa-times"></span> Reset</a>
											</span>
										</div>
									@else  
										<input type="text" name="cari" id="cari" class="form-control" placeholder="Cari Data Pengguna..." style="border-radius: 0px;" value="{{ request()->get('cari') ? request()->get('cari'):'' }}">
									@endif
								</div>
							</div>
						</form>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th class="text-center">No.</th>
										<th>Nama Pengguna</th>
										<th>E-Mail</th>
										<th>Hak Akses</th>
										<th>Tanggal Buat</th>
										<th>Terakhir Login</th>
										<th class="text-center">Aksi</th>
									</tr>
								</thead>
								<tbody>
									@php
										if (request()->has('page')) {
											$no = request()->get('page') * 10 - 10 + 1;
										} else {
											$no = 1;
										}
									@endphp
									@forelse ($user as $item)
										<tr>
											<td class="text-center" style="font-weight: 600; padding-top: 12px;">{{ $no++ }}.</td>
											<td style="padding-top: 12px;">{{ $item->first_name }} {{ $item->last_name }}</td>
											<td style="padding-top: 12px;">{{ $item->email }}</td>
											<td style="padding-top: 12px;">{{ $item->roles->first()->name }}</td>
											<td style="padding-top: 12px;">{{ date('d/m/Y H:i:s', strtotime($item->created_at)) }}</td>
											<td style="padding-top: 12px;">{{ date('d/m/Y H:i:s', strtotime($item->last_login_at)) }}</td>
											<td class="text-center" style="min-width: 150px !important;">
												<div class="btn-group">
													<a href="{{ route('Users.show', $item->id) }}" class="btn btn-sm btn-info flat">
														<i class="fa fa-arrow-right"></i>
													</a>
													<a href="{{ route('Users.edit', $item->id) }}" class="btn btn-sm btn-warning flat">
														<i class="fa fa-edit"></i>
													</a>
													<a href="javascript:void(0)" class="btn btn-sm btn-danger" onclick="event.preventDefault(); document.getElementById('destroy'+{{ $item->id }}).submit();">
														<i class="fa fa-trash"></i>
													</a>
													<form action="{{ route('Users.destroy', $item->id) }}" method="post" id="destroy{{ $item->id }}">
														@csrf
														@method('DELETE')
													</form>
												</div>
											</td>
										</tr>
									@empty
										<tr>
											<td colspan="6" class="text-center">Belum Ada Data Pengguna.</td>
										</tr>
									@endforelse
								</tbody>
								<tfoot>
									<tr>
										<td colspan="6" class="text-right">
											{{ $user->links() }}
										</td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="trashed-modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Tempat Sampah Data Pengguna</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive" style="max-height: 500px;">
					<table class="table table-stripped">
						<thead>
							<tr>
								<th class="text-center">No.</th>
								<th>Nama Pengguna</th>
								<th>E-Mail</th>
								<th>Hak Akses</th>
								<th>Tanggal Hapus</th>
								<th class="text-center">Aksi</th>
							</tr>
						</thead>
						<tbody>
							@forelse ($tuser as $item)
								<tr>
									<td class="text-center" style="font-weight: 600; padding-top: 12px;">{{ $loop->iteration }}.</td>
									<td style="padding-top: 12px;">{{ $item->first_name }} {{ $item->last_name }}</td>
									<td style="padding-top: 12px;">{{ $item->email }}</td>
									<td style="padding-top: 12px;">{{ $item->roles->first()->name }}</td>
									<td style="padding-top: 12px;">{{ date('d/m/Y H:i:s', strtotime($item->deleted_at)) }}</td>
									<td class="text-center" style="min-width: 150px !important;">
										<div class="btn-group">
											<a href="javascript:void(0)" class="btn btn-sm btn-success" onclick="event.preventDefault(); document.getElementById('restore'+{{ $item->id }}).submit();">
												<i class="fa fa-undo"></i> Pulihkan
											</a>
											<a href="javascript:void(0)" class="btn btn-sm btn-danger" onclick="event.preventDefault(); document.getElementById('perma'+{{ $item->id }}).submit();">
												<i class="fa fa-trash"></i> Hapus
											</a>
											
											<form action="{{ route('Users.restore', $item->id) }}" method="post" id="restore{{ $item->id }}">
													@csrf
													@method('PUT')
												</form>
											<form action="{{ route('Users.perma', $item->id) }}" method="post" id="perma{{ $item->id }}">
												@csrf
												@method('DELETE')
											</form>
										</div>
									</td>
								</tr>
							@empty
								
							@endforelse
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer justify-content-between">
				<button type="button" class="btn btn-outline-light" data-dismiss="modal">Tutup</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

@endsection