@extends('backend.app')

@section('content')
	<div class="row" style="margin-top: 15px;">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-user"></i>
						<span class="caption-subject bold"> &ensp; Detail Pengguna</span>
					</div>
					<div class="actions">
						<div class="btn-group">
							<a href="{{ route('Users.edit', $user->id) }}" class="btn btn-sm btn-warning">
								<i class="fa fa-pencil"></i> &ensp; Edit Data Pengguna
							</a>
							<a href="{{ route('Users.index') }}" class="btn red">
								<i class="fa fa-times"></i> &ensp; Kembali
							</a>
						</div>
					</div>
				</div>
				<div class="portlet-body">
					<div class="hidden-xs hidden-sm" >
						<div class="row">
							<div class="col-md-3 col-lg-3 text-center">
								<img src="{{ asset('images/users') }}/{{ $user->photo }}" alt="{{ $user->first_name }}-{{ $user->photo }}" class="img-responsive center-block" style="max-height: 200px !important;">
								<h5>
									{{ $user->first_name }}
									{{ $user->last_name }}
								</h5>
								<h5>
									- {{ $user->roles->first()->name }}  -
								</h5>
								<hr style="margin: 0px; padding:0px;">
								<h5>Terakhir Login</h5>
								<h5>{{ date('H:i:s d/m/Y', strtotime($user->last_login_at)) }}</h5>
								<h5>IP : {{ $user->last_login_ip }}</h5>
							</div>
							<div class="col-md-9 col-lg-9">
								<div class="panel panel-primary" style="border-radius: 0px;">
									<div class="panel-heading clearfix" style="border-radius: 0px;">
										{{-- <div class="btn-group pull-right">
											<a href="#" data-toggle="collapse" data-target="#data-body" style="color: white; font-wight: 800;">
												<i class="fa fa-angle-down"></i>
											</a>
										</div> --}}
										<h4 class="panel-title">Data Diri Pengguna</h4>
									</div>
									<div class="panel-body">
										<div class="row">
											<div class="col-md-3 col-lg-3">
												Nama Depan
											</div>
											<div class="col-md-1 col-lg-1">:</div>
											<div class="col-md-8 col-lg-8">
												{{ $user->first_name }}
											</div>
											<div class="col-md-3 col-lg-3" style="margin-top: 4px;">
												Nama Belakang
											</div>
											<div class="col-md-1 col-lg-1" style="margin-top: 4px;">:</div>
											<div class="col-md-8 col-lg-8" style="margin-top: 4px;">
												{{ $user->last_name }}
											</div>
											<div class="col-md-3 col-lg-3" style="margin-top: 4px;">
												E-Mail
											</div>
											<div class="col-md-1 col-lg-1" style="margin-top: 4px;">:</div>
											<div class="col-md-8 col-lg-8" style="margin-top: 4px;">
												{{ $user->email }}
											</div>
											<div class="col-md-3 col-lg-3" style="margin-top: 4px;">
												Negara
											</div>
											<div class="col-md-1 col-lg-1" style="margin-top: 4px;">:</div>
											<div class="col-md-8 col-lg-8" style="margin-top: 4px;">
												{{ $user->country->name }}
											</div>
											<div class="col-md-3 col-lg-3" style="margin-top: 4px;">
												Jenis Kelamin
											</div>
											<div class="col-md-1 col-lg-1" style="margin-top: 4px;">:</div>
											<div class="col-md-8 col-lg-8" style="margin-top: 4px;">
												{{ $user->jenis_kelamin($user->gender) }}
											</div>
											<div class="col-md-3 col-lg-3" style="margin-top: 4px;">
												Tanggal Lahir
											</div>
											<div class="col-md-1 col-lg-1" style="margin-top: 4px;">:</div>
											<div class="col-md-8 col-lg-8" style="margin-top: 4px;">
												{{ date('d/m/Y', strtotime($user->date_of_birth)) }}
											</div>
											<div class="col-md-3 col-lg-3" style="margin-top: 4px;">
												Nomor Telfon
											</div>
											<div class="col-md-1 col-lg-1" style="margin-top: 4px;">:</div>
											<div class="col-md-8 col-lg-8" style="margin-top: 4px;">
												(+62) {{ $user->phone }}
											</div>
											<div class="col-md-3 col-lg-3" style="margin-top: 4px;">
												Alamat
											</div>
											<div class="col-md-1 col-lg-1" style="margin-top: 4px;">:</div>
											<div class="col-md-8 col-lg-8" style="margin-top: 4px;">
												{{ $user->address }}
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="hidden-md-hidden-lg">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hidden-md hidden-lg">
								XS & SM
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
	<script>
		$(document).ready(function() {
			//
		});
	</script>
@endsection