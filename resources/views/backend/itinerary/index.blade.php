@extends('backend.app')

@section('css')
<style>
	.flat {
		border-radius: 0px !important;
	}
</style>
@endsection

@section('content')
<div class="row" style="margin-top: 15px;">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-plane"></i>
					<span class="caption-subject bold"></span> &ensp; Atur Data Perjalanan Tur
				</div>
				<div class="actions">
					<div class="btn-group">
						<a href="{{ route('Package.show', $package->id) }}" class="btn red">
							<i class="fa fa-times"></i> &ensp; Kembali
						</a>
					</div>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-3 col-sm-3 col-xs-3">
						<ul class="nav nav-tabs tabs-left">
							@for ($i = 1; $i <= $package->duration; $i++)
							<li class="{{ $i == 1 ? 'active':'' }}">
								<a href="#tab_6_{{ $i }}" data-toggle="tab" aria-expanded="true"> Hari Ke-{{ $i }} </a>
							</li>
							@endfor
						</ul>
					</div>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<div class="tab-content">
							@for ($i = 1; $i <= $package->duration; $i++)
							@php
								$name = '';
								$transport = '';
								$hotel = '';
								$meals = '';
								$desc = '';
								
								foreach ($package->itinerary as $item) {
									if ($item->day == $i) {
										$name = $item->name;
										$transport = $item->trasport;
										$hotel = $item->hotel;
										$meals = $item->meals;
										$desc = $item->description;
									}
								}
							@endphp
							<div class="tab-pane {{ $i == 1 ? 'active':'' }} in" id="tab_6_{{ $i }}">
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<h4 class="bold"> <span class="fa fa-calendar"></span> &ensp; Form Perjalanan Hari ke-{{$i}}</h4>
										<hr>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="form-group" id="fname{{$i}}">
											<label class="bold">Nama Perjalanan &ensp; : <i class="text-danger">*</i></label>
											<input type="text" name="name{{$i}}" id="name{{$i}}" class="form-control flat" required placeholder="Masukan Nama Perjalanan...">
											<span class="text-danger help-block" id="alname{{$i}}"></span>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
										<div class="form-group" id="ftransport{{$i}}">
											<label class="bold">Transportasi &ensp; : </label>
											<input type="text" name="transport{{$i}}" id="transport{{$i}}" class="form-control flat" required placeholder="Masukan Transportasi...">
											<span class="text-danger help-block" id="altransport{{$i}}"></span>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
										<div class="form-group" id="fhotel{{$i}}">
											<label class="bold">Hotel &ensp; : </label>
											<input type="text" name="hotel{{$i}}" id="hotel{{$i}}" class="form-control flat" required placeholder="Masukan Hotel...">
											<span class="text-danger help-block" id="alhotel{{$i}}"></span>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="form-group" id="fmeals{{$i}}">
											<label class="bold">Makan &ensp; : </label>
											<input type="text" name="meals{{$i}}" id="meals{{$i}}" class="form-control flat" required placeholder="Makan Pagi, Makan Siang, Makan Malam...">
											<span class="help-block">
												Pisahkan dengan (,) koma.
											</span>
											<span class="text-danger help-block" id="almeals{{$i}}"></span>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="form-group" id="fdescription{{$i}}">
											<label class="bold">Deskripsi Tur : </label>
											<textarea name="description" id="description{{$i}}" rows="3" class="form-control flat" placeholder="Masukan Deskripsi Rencana Perjalanan..." required>{{ $desc }}</textarea>
											<span class="text-danger help-block" id="aldescription{{$i}}"></span>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
										<button class="btn btn-sm green btn-block flat save" name="save" data-day="{{$i}}">
											<i class="fa fa-plus"></i> &ensp; Simpan
										</button>
									</div>
								</div>
							</div>
							@endfor
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')

<script>
	$(document).ready(function() {
		$('.save').on('click', function() {
			var package_id = "{{ $package->id }}";
			var day = $(this).data('day');
			var name = $('#name'+day).val();
			var transport = $('#transport'+day).val();
			var hotel = $('#hotel'+day).val();
			var meals = $('#meals'+day).val();
			var description = $('#description'+day).val();

			$.ajax({
				url: "{{ route('Itinerary.store') }}",
				method: "POST",
				data: {
					_token: "{{ csrf_token() }}",
					package_id: package_id,
					day: day,
					name: name,
					transport: transport,
					hotel: hotel,
					meals: meals,
					description: description, 
				},
				statusCode: {
					422: function(e) {
						var x = e.responseJSON.errors;
						Object.keys(x).forEach(function eachKey(key) {
							$('#f'+key+day).addClass('has-error');
							$('#al'+key+day).text(x[key]);
						});
					}
				},
				success: function(res) {
					var code = res.code;
					if (code == 'error') {
						toastError(res.message);
					} else if (code == 'success') {
						toastSuccess(res.message);
					}
				}
			});

		}); 
	});
</script>
@endsection