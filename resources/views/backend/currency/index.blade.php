@extends('backend.app')

@section('content')
<div class="row" style="margin-top: 15px;">
	<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-money"></i>
					<span class="caption-subject bold"> &ensp; Form Data Mata Uang</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						@if (isset($edit))
						<form action="{{ route('Currency.update', $edit->id) }}" method="post" class="form">						
							@method('PUT')
						@else
						<form action="{{ route('Currency.store') }}" method="post" class="form">
						@endif
							@csrf
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<div class="form-group {{ $errors->has('code') ? 'has-error':'' }}">
										<label class="bold">Kode Mata Uang : <i class="text-danger">*</i></label>
										<input type="text" name="code" id="code" class="form-control flat" placeholder="Masukan Kode Mata Uang..." value="{{ isset($edit) ? $edit->code:old('code') }}" autofocus="" style="margin-top: 6px;" required>
										<span class="help-block">
											{{ $errors->first('code') }}
										</span>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
									<div class="form-group {{ $errors->has('name') ? 'has-error':'' }}">
										<label class="bold">Nama Mata Uang : <i class="text-danger">*</i></label>
										<input type="text" name="name" id="name" class="form-control flat" placeholder="Masukan Nama Mata Uang..." value="{{ isset($edit) ? $edit->name:old('name') }}" autofocus="" style="margin-top: 6px;" required>
										<span class="help-block">
											{{ $errors->first('name') }}
										</span>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="form-group {{ $errors->has('description') ? 'has-error':'' }}">
										<label class="bold">Deskripsi Mata Uang : </label>
										<textarea name="description" id="description" rows="3" class="form-control flat" placeholder="Masukan Deskripsi Mata Uang..." style="margin-top: 6px;" >{{ isset($edit) ? $edit->description:old('description') }}</textarea>
										<span class="help-block">
											{{ $errors->first('description') }}
										</span>
									</div>
								</div>
							</div>
							<hr style="margin-bottom: 12px;">
							<div class="form-group">
								@if (isset($edit))
								<button type="submit" class="btn green flat" style="margin: 3px;">
									<i class="fa fa-plus"></i> &ensp; Tambah Data
								</button>
								<button type="reset" class="btn btn-warning flat" style="margin: 3px;">
									<i class="fa fa-undo"></i> &ensp; Reset Input
								</button>
								<a href="{{ route('Currency.index') }}" class="btn btn-danger flat" style="margin: 3px;">
									<i class="fa fa-times"></i> &ensp; Batal
								</a>
								@else
								<button type="submit" class="btn green flat" style="margin: 3px;">
									<i class="fa fa-plus"></i> &ensp; Tambah Data
								</button>
								<button type="reset" class="btn btn-danger flat" style="margin: 3px;">
									<i class="fa fa-undo"></i> &ensp; Reset Input
								</button>
								@endif
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-money"></i>
					<span class="caption-subject bold"> &ensp; Kode Mata Uang Dunia</span>
				</div>
			</div>
			<div class="portlet-body" style="max-height: 290px; overflow: auto;">
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th>Kode Mata Uang</th>
							</tr>
						</thead>
						<tbody id="world-code">

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-money"></i>
					<span class="caption-subject bold"> &ensp; Data Mata Uang </span>
				</div>
				<div class="actions">
					<div class="btn-group">
						<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#trashed-modal">
							<i class="fa fa-trash"></i> &ensp; Tong Sampah 
						</button>
					</div>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<form action="{{ url('Backend/Users') }}" method="get" class="form-horizontal">
							<div class="form-group row">
								<label for="cari" class="col-xs-12 col-sm-2 col-md-4 col-lg-4 control-label text-left">Cari Data :</label>
								<div class="col-xs-12 col-sm-10 col-md-8 col-lg-8">
									@if (request()->has('cari'))
										<div class="input-group">
											<input type="text" name="cari" id="cari" class="form-control" placeholder="Cari Data Mata Uang..." style="border-radius: 0px;" value="{{ request()->get('cari') ? request()->get('cari'):'' }}">
											<span class="input-group-addon" style="border-radius: 0px !important;">
												<a href="{{ route('Currency.index') }}" style="border-radius: 0px; text-decoration: none !important;"><span class="fa fa-times"></span> Reset</a>
											</span>
										</div>
									@else 
										<input type="text" name="cari" id="cari" class="form-control" placeholder="Cari Data Mata Uang..." style="border-radius: 0px;" value="{{ request()->get('cari') ? request()->get('cari'):'' }}">
									@endif
								</div>
							</div>
						</form>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th class="text-center">No.</th>
										<th>Kode</th>
										<th>Nama Mata Uang</th>
										<th>Deskripsi</th>
										<th>Tanggal Buat</th>
										<th class="text-center">Aksi</th>
									</tr>
								</thead>
								<tbody>
									@php
										if (request()->has('page')) {
											$no = request()->get('page') * 10 - 10 + 1;
										} else {
											$no = 1;
										}
									@endphp
									@forelse ($currency as $item)
										<tr>
											<td class="text-center" style="font-weight: 600; padding-top: 12px;">{{ $no++ }}.</td>
											<td style="padding-top: 12px;">{{ $item->code }} </td>
											<td style="padding-top: 12px;">{{ $item->name }} </td>
											<td style="padding-top: 12px;">{{ $item->description == null ? 'Tidak Ada Deskripsi':$item->description }} </td>
											<td style="padding-top: 12px;" width="18%">{{ date('d/m/Y H:i:s', strtotime($item->created_at)) }}</td>
											<td class="text-center" style="min-width: 100px !important;">
												<div class="btn-group">
													<a href="{{ route('Currency.edit', $item->id) }}" class="btn btn-sm btn-warning flat">
														<i class="fa fa-edit"></i>
													</a>
													<a href="javascript:void(0)" class="btn btn-sm btn-danger" onclick="event.preventDefault(); document.getElementById('destroy'+{{ $item->id }}).submit();">
														<i class="fa fa-trash"></i>
													</a>
													<form action="{{ route('Currency.destroy', $item->id) }}" method="post" id="destroy{{ $item->id }}">
														@csrf
														@method('DELETE')
													</form>
												</div>
											</td>
										</tr>
									@empty
										<tr>
											<td colspan="4" class="text-center">Belum Ada Data Mata Uang.</td>
										</tr>
									@endforelse
								</tbody>
								<tfoot>
									<tr>
										<td colspan="6" class="text-right">
											{{ $currency->links() }}
										</td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="trashed-modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Tempat Sampah Data Mata Uang</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive" style="max-height: 500px;">
					<table class="table">
						<thead>
							<tr>
								<th class="text-center">No.</th>
								<th>Nama Mata Uang</th>
								<th>Tanggal Hapus</th>
								<th class="text-center">Aksi</th>
							</tr>
						</thead>
						<tbody>
							@forelse ($tcurrency as $item)
								<tr>
									<td class="text-center" style="font-weight: 600; padding-top: 12px;">{{ $loop->iteration }}.</td>
									<td style="padding-top: 12px;">{{ $item->name }} </td>
									<td style="padding-top: 12px;" width="18%">{{ date('d/m/Y H:i:s', strtotime($item->deleted_at)) }}</td>
									<td class="text-center" style="min-width: 100px !important;">
										<div class="btn-group">
											<a href="javascript:void(0)" class="btn btn-sm btn-success flat" onclick="event.preventDefault(); document.getElementById('restore'+{{ $item->id }}).submit();">
												<i class="fa fa-undo"></i> Pulihkan
											</a>
											<a href="javascript:void(0)" class="btn btn-sm btn-danger" onclick="event.preventDefault(); document.getElementById('perma'+{{ $item->id }}).submit();">
												<i class="fa fa-trash"></i> Hapus
											</a>
											
											<form action="{{ route('Currency.restore', $item->id) }}" method="post" id="restore{{ $item->id }}">
												@csrf
												@method('PUT')
											</form>
											<form action="{{ route('Currency.perma', $item->id) }}" method="post" id="perma{{ $item->id }}">
												@csrf
												@method('DELETE')
											</form>
										</div>
									</td>
								</tr>
							@empty
								<tr>
									<td colspan="4" class="text-center">Belum Ada Data Yang Pernah di-Hapus.</td>
								</tr>
							@endforelse
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer justify-content-between">
				<button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-outline-light">Save changes</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

@endsection

@section('script')
<script>
	$(document).ready(function() {
		function getWorldCurrency() {
			$.ajax({
				url: "http://data.fixer.io/api/latest?access_key=45323873155f3d336d9e332b4bf0fbc1&format=1",
				method: "GET",
				success: function(data) {
					rates = data.rates;
					Object.keys(rates).forEach(function(item) {
						$('#world-code').append(`
							<tr>
								<td>${item}</td>
							</tr>
						`);
					});
				}
			});
		// http://data.fixer.io/api/latest?access_key=45323873155f3d336d9e332b4bf0fbc1&format=1
		}

		getWorldCurrency();
	});
</script>
@endsection