<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Itinerary extends Model
{
  protected $fillable = [
		'package_id', 'day', 'name', 'transport', 'hotel', 'meals', 'description' 
	];	

	public function package()
	{
		return $this->belongsTo('App\Package');
	}

}
