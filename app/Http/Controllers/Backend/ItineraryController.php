<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Itinerary;
use App\Package;
use Illuminate\Http\Request;

class ItineraryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
      try {
				$package = Package::findOrFail($id);
				return view('backend.itinerary.index', compact('package', 'itinerary'));
			} catch (\Exception $e) {
				abort(404);
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back();
			}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
				'package_id' => 'required|numeric|exists:packages,id',
				'day' => 'required|numeric',
				'name' => 'required|string',
				'transport' => 'nullable|string',
				'hotel' => 'nullable|string',
				'meals' => 'nullable|string',
				'description' => 'required|string',
			]);

			try {
				$package = Package::findOrFail($request->package_id);
				$itinerary = Itinerary::where('package_id', '=', $request->package_id)->where('day', '=' ,$request->day)->first();
				if ($itinerary != null) {
					$itinerary->update([
						'name' => $request->name,
						'transport' => $request->transport,
						'hotel' => $request->hotel,
						'meals' => $request->meals,
						'description' => $request->description
					]);

					$data = [
						'code' => 'success',
						'message' => 'Data Perjalanan di-Ubah !',
					];
					return response()->json($data, 200);
				} else {
					if ($request->day > $package->duration) {
						$data = [
							'code' => 'error',
							'message' => 'Terjadi Kesalahan, Silahkan Refresh Ulang Halaman !',
						];
						return response()->json($data, 200);
					} else {
						$itinerary = Itinerary::create([
							'package_id' => $request->package_id,
							'day' => $request->day,
							'name' => $request->name,
							'hotel' => $request->hotel,	
							'transport' => $request->transport,
							'meals' => $request->meals,
							'description' => $request->description
						]);

						$data = [
							'code' => 'success',
							'message' => 'Data Perjalanan di-Simpan !',
						];
						return response()->json($data, 200);
					}
				}
			} catch (\Exception $e) {
				$data = [
					'code' => 'error',
					'message' => 'Terjadi Kesalahan, Hubungi Operator !',
				];
				return response()->json($data);
			}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
