<?php

namespace App\Http\Controllers\Backend;

use App\Currency;
use App\Http\Controllers\Controller;
use App\Package;
use App\Type;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
			$package = Package::orderBy('created_at', 'DESC')->paginate(10);
			$tpackage = Package::orderBy('deleted_at')->onlyTrashed()->get();
			return view('backend.package.index', compact('package', 'tpackage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
			$type = Type::orderBy('name', 'ASC')->get();
			$currency = Currency::orderBy('code', 'ASC')->get();
      return view('backend.package.create', compact('type', 'currency'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
			$this->validate($request, [
				'name' => 'required|string',
				'type_id' => 'required|numeric|exists:types,id',
				'duration' => 'required|numeric|min:1',
				'minimum' => 'required|numeric|min:1',
				'currency_id' => 'required|numeric|exists:currencies,id',
				'expired_date' => 'required|string',
				'description' => 'required|string',
			]);

			try {
				$tgl = '';
				$tgl = explode('/', $request->expired_date);
				if (count($tgl) != '3') {
					return redirect()->back()->withInput($request->all());
					session()->flash('error', 'Format Tanggal Salah !');
				} else {
					$tgl = $tgl[2] . '-' . $tgl[1] . '-' . $tgl[0];
					$package = Package::firstOrCreate([
						'name' => $request->name,
						'type_id' => $request->type_id,
						'duration' => $request->duration,
						'minimum' => $request->minimum,
						'currency_id' => $request->currency_id,
						'expired_date' => $tgl,
						'description' => $request->description,
					]);

					session()->flash('success', 'Data Paket Tur di-Tambahkan !');

					if ($request->has('detail')) {
						return redirect(route('Package.show', $package->id));
					} else if ($request->has('again')) {
						return redirect(route('Package.create'));
					} else {
						return redirect(route('Package.index'));
					}
				}
			} catch (\Exception $e) {
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back()->withInput();
			}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      try {
				$package = Package::findOrFail($id);
				return view('backend.package.show', compact('package'));
			} catch (\Exception $e) {
				abort(404);
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back();
			}
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
			try {
				$edit = Package::findOrFail($id);
				$type = Type::orderBy('name', 'ASC')->get();
				$currency = Currency::orderBy('code', 'ASC')->get();
				return view('backend.package.edit', compact('type', 'currency', 'edit'));
			} catch (\Exception $e) {
				abort(404);
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back();
			}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
			$this->validate($request, [
				'name' => 'required|string',
				'type_id' => 'required|numeric|exists:types,id',
				'duration' => 'required|numeric|min:1',
				'minimum' => 'required|numeric|min:1',
				'currency_id' => 'required|numeric|exists:currencies,id',
				'expired_date' => 'required|string',
				'description' => 'required|string',
			]);

			try {
				$package = Package::findOrFail($id);
				$tgl = '';
				$tgl = explode('/', $request->expired_date);
				if (count($tgl) != '3') {
					return redirect()->back()->withInput($request->all());
					session()->flash('error', 'Format Tanggal Salah !');
				} else {
					$tgl = $tgl[2] . '-' . $tgl[1] . '-' . $tgl[0];
					$package->update([
						'name' => $request->name,
						'type_id' => $request->type_id,
						'duration' => $request->duration,
						'minimum' => $request->minimum,
						'currency_id' => $request->currency_id,
						'expired_date' => $tgl,
						'description' => $request->description,
					]);

					session()->flash('info', 'Data Paket Tur di-Ubah !');
					return redirect(route('Package.index'));
				}
			} catch (\Exception $e) {
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back();
			}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      try {
				$package = Package::findOrFail($id);
				$package->delete();

				session()->flash('warning', 'Paket Tur di-Non Aktifkan !');
				return redirect(route('Package.index'));
			} catch (\Exception $e) {
				abort(404);
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back();
			}
		}
		
		public function restore(Request $request, $id)
		{
			try {
				$package = Package::onlyTrashed()->where('id', '=', $id)->firstOrFail();
				$package->restore();
				session()->flash('success', 'Paket Tur di-Pulihkan !');
				return redirect(route('Package.index'));
			} catch (\Exception $e) {
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back();
			}
		}

		public function permanentDelete(Request $request, $id)
		{
			try {
				$package = Package::onlyTrashed()->where('id', '=', $id)->firstOrFail();
				$package->forceDelete();
				session()->flash('warning', 'Data Paket Tur di-Hapus Permanen !');
				return redirect(route('Package.index'));
			} catch (\Exception $e) {
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back();
			}
		}
}
