<?php

namespace App\Http\Controllers\Backend;

use App\Currency;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
				$currency = Currency::orderBy('created_at', 'DESC')->paginate(10);
				$tcurrency = Currency::orderBy('deleted_at')->onlyTrashed()->get();
				return view('backend.currency.index', compact('currency', 'tcurrency'));
		}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
					'code' => 'required|string|unique:currencies,code',
					'name' => 'required|string',
					'description' => 'nullable|string'
				]);

				try {
					$currency = Currency::firstOrCreate([
						'code' => $request->code,
						'name' => $request->name,
						'description' => $request->description,
					]);

					session()->flash('success', 'Data Mata Uang di-Tambahkan !');
					return redirect(route('Currency.index'));
				} catch (\Exception $e) {
					abort(404);
					session()->flash('error', 'Terjadi Kesalahan !');
					return redirect()->back();
				}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
			try {
				$edit = Currency::findOrFail($id);
				$currency = Currency::orderBy('created_at', 'DESC')->paginate(10);
				$tcurrency = Currency::orderBy('deleted_at')->onlyTrashed()->get();
				return view('backend.currency.index', compact('currency', 'tcurrency', 'edit'));
			} catch (\Exception $e) {
				abort(404);
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back();
			}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
				'code' => 'required|string|unique:currencies,code,'.$id,
				'name' => 'required|string',
				'description' => 'nullable|string', 
			]);

			try {
				$currency = Currency::findOrFail($id);
				$currency->update([
					'code' => $request->code,
					'name' => $request->name,
					'description' => $request->description
				]);

				session()->flash('info', 'Data Mata Uang di-Ubah !');
				return redirect(route('Currency.index'));
			} catch (\Exception $e) {
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back();
			}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      try {
				$currency = Currency::findOrFail($id);
				$currency->delete();
				session()->flash('warning', 'Data Mata Uang di-Hapus !');
				return redirect(route('Currency.index'));
			} catch (\Exception $e) {
				abort(404);
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back();
			}
		}
		
		public function restore(Request $request, $id)
		{
			try {
				$currency = Currency::onlyTrashed()->where('id', '=', $id)->firstOrFail();
				$currency->restore();
				session()->flash('success', 'Jenis Tur di-Pulihkan !');
				return redirect(route('Currency.index'));
			} catch (\Exception $e) {
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back();
			}
		}

		public function permanentDelete(Request $request, $id)
		{
			try {
				$currency = Currency::onlyTrashed()->where('id', '=', $id)->firstOrFail();
				$currency->forceDelete();
				session()->flash('warning', 'Data Jenis Tur di-Hapus Permanen !');
				return redirect(route('Currency.index'));
			} catch (\Exception $e) {
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back();
			}
		}
}
