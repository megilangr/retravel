<?php

namespace App\Http\Controllers\Backend;

use App\Country;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
			if (request()->get('cari')) {
				$cari = request()->get('cari');
				$user = User::orderBy('created_at', 'DESC')
					->where('first_name', 'LIKE', '%'.$cari.'%')
					->orwhere('last_name', 'LIKE', '%'.$cari.'%')
					->orwhere('email', 'LIKE', '%'.$cari.'%')
					->paginate(10);
			} else {
				$user = User::orderBy('created_at', 'DESC')->paginate(10);
			}
			$tuser = User::orderBy('deleted_at')->onlyTrashed()->get();
      return view('backend.user.index', compact('user', 'tuser'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
			$role = Role::orderBy('name', 'ASC')->get();
			$country = Country::where('code', '!=', 'UC')->orderBy('name', 'ASC')->get();
      return view('backend.user.create', compact('role', 'country'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
				'first_name' => 'required|string',
				'last_name' => 'required|string',
				'country_id' => 'required|numeric|exists:countries,id',
				'date_of_birth' => 'required|string',
				'gender' => 'required|numeric|digits:1',
				'phone' => 'required|numeric',
				'address' => 'required|string',
				'email' => 'required|string|email|confirmed|unique:users,email',
				'password' => 'required|string|min:8|confirmed',
				'photo' => 'nullable|image|mimes:jpg,jpeg,png',
				'roles' => 'required|string|exists:roles,name'
			]);

			try {
				$tgl = '';
				$tgl = explode('/', $request->date_of_birth);
				if (count($tgl) != '3') {
					return redirect()->back()->withInput($request->all());
					session()->flash('error', 'Format Tanggal Salah !');
				} else {
					$photo = null;
					if ($request->hasFile('photo')) {
						$photo = time() . rand(10, 99) . '.' . $request->file('photo')->getClientOriginalExtension();
						$request->file('photo')->storeAs('users', $photo, 'images');
					}

					$tgl = $tgl[2] . '-' . $tgl[1] . '-' . $tgl[0];

					$user = User::firstOrCreate([
						'first_name' => $request->first_name,
						'last_name' => $request->last_name,
						'country_id' => $request->country_id,
						'date_of_birth' => $tgl,
						'gender' => $request->gender,
						'phone' => $request->phone,
						'address' => $request->address,
						'photo' => $photo,
						'email' => $request->email,
						'password' => bcrypt($request->password)
					]);
					$user->assignRole($request->roles);

					session()->flash('success', 'Data User Di-Tambahkan !');
					return redirect(route('Users.index'));
				}
			} catch (\Exception $e) {
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back()->withInput($request->all());
			}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      try {
				$user = User::findOrFail($id);
				return view('backend.user.show', compact('user'));
			} catch (\Exception $e) {
				abort(404);
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back();
			}
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
			try {
				$edit = User::findOrFail($id);
				$role = Role::orderBy('name', 'ASC')->get();
				$country = Country::where('code', '!=', 'UC')->orderBy('name', 'ASC')->get();
				return view('backend.user.edit', compact('edit', 'role', 'country'));
			} catch (\Exception $e) {
				abort(404);
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back();
			}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
				'first_name' => 'required|string',
				'last_name' => 'required|string',
				'country_id' => 'required|numeric|exists:countries,id',
				'date_of_birth' => 'required|string',
				'gender' => 'required|numeric|digits:1',
				'phone' => 'required|numeric',
				'address' => 'required|string',
				'photo' => 'nullable|image|mimes:jpg,jpeg,png',
				'roles' => 'required|string|exists:roles,name'
			]);

			try {
				$user = User::findOrFail($id);
				$password = $user->password;
				if ($request->password != null) {
					$this->validate($request, [
						'password' => 'required|string|min:8|confirmed',
					]);
					$password = bcrypt($request->password);
				} 

				$tgl = '';
				$tgl = explode('/', $request->date_of_birth);
				if (count($tgl) != '3') {
					return redirect()->back()->withInput($request->all());
					session()->flash('error', 'Format Tanggal Salah !');
				} else {
					$photo = $user->photo;
					if ($request->hasFile('photo')) {
						$photo = time() . rand(10, 99) . '.' . $request->file('photo')->getClientOriginalExtension();
						$request->file('photo')->storeAs('users', $photo, 'images');
						Storage::disk('images')->delete('users/'.$user->photo);
					}

					$tgl = $tgl[2] . '-' . $tgl[1] . '-' . $tgl[0];
					
					$user->update([
						'first_name' => $request->first_name,
						'last_name' => $request->last_name,
						'country_id' => $request->country_id,
						'date_of_birth' => $tgl,
						'gender' => $request->gender,
						'phone' => $request->phone,
						'address' => $request->address,
						'photo' => $photo,
						'password' => $password,
					]);
					$user->assignRole($request->roles);

					session()->flash('info', 'Data User Di-Ubah !');
					return redirect(route('Users.index'));
				}
			} catch (\Exception $e) {
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back();
			}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      try {
				$user = User::findOrFail($id);
				$user->delete();
				session()->flash('warning', 'Data Pengguna di-Hapus !');
				return redirect(route('Users.index'));
			} catch (\Exception $e) {
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back();
			}
		}
		
		public function restore(Request $request, $id)
		{
			try {
				$user = User::onlyTrashed()->where('id', '=', $id)->firstOrFail();
				$user->restore();
				session()->flash('success', 'Pengguna di-Pulihkan !');
				return redirect(route('Users.index'));
			} catch (\Exception $e) {
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back();
			}
		}

		public function permanentDelete(Request $request, $id)
		{
			try {
				$user = User::onlyTrashed()->where('id', '=', $id)->firstOrFail();
				$user->forceDelete();
				if ($user->photo != null) {
					Storage::disk('images')->delete('users/'.$user->photo);
				}

				session()->flash('warning', 'Data Pengguna di-Hapus Permanen !');
				return redirect(route('Users.index'));
			} catch (\Exception $e) {
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back();
			}
		}
}
