<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Type;
use Illuminate\Http\Request;

class TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
			$type = Type::orderBy('created_at', 'DESC')->paginate(10);
			$ttype = Type::orderBy('deleted_at')->onlyTrashed()->get();
			return view('backend.type.index', compact('type', 'ttype'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
				//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
			$this->validate($request, [
				'name' => 'required|string|unique:types,name|max:50'
			]);

			try {
				$type = Type::firstOrCreate([
					'name' => $request->name
				]);

				session()->flash('success', 'Data Jenis Tur di-Tambahkan !');
				return redirect(route('Type.index'));
			} catch (\Exception $e) {
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back()->withInput();
			}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      try {
				$edit = Type::findOrFail($id);
				$type = Type::orderBy('created_at', 'DESC')->paginate(10);
				$ttype = Type::orderBy('deleted_at')->onlyTrashed()->get();

				return view('Backend.type.index', compact('type', 'ttype', 'edit'));
			} catch (\Exception $e) {
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back();
			}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
			$this->validate($request, [
				'name' => 'required|string|unique:types,name,'.$id,
			]);

      try {
				$type = Type::findOrFail($id);
				$type->update([
					'name' => $request->name,
				]);

				session()->flash('info', 'Data Jenis Tur di-Ubah !');
				return redirect(route('Type.index'));
			} catch (\Exception $e) {
				return redirect()->back();
			}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
			try {
				$type = Type::findOrFail($id);
				$type->delete();
				session()->flash('warning', 'Data Jenis di-Hapus !');
				return redirect(route('Type.index'));
			} catch (\Exception $e) {
				abort(404);
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back();
			}
		}
		
		public function restore(Request $request, $id)
		{
			try {
				$type = Type::onlyTrashed()->where('id', '=', $id)->firstOrFail();
				$type->restore();
				session()->flash('success', 'Jenis Tur di-Pulihkan !');
				return redirect(route('Type.index'));
			} catch (\Exception $e) {
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back();
			}
		}

		public function permanentDelete(Request $request, $id)
		{
			try {
				$type = Type::onlyTrashed()->where('id', '=', $id)->firstOrFail();
				$type->forceDelete();
				session()->flash('warning', 'Data Jenis Tur di-Hapus Permanen !');
				return redirect(route('Type.index'));
			} catch (\Exception $e) {
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back();
			}
		}
}
