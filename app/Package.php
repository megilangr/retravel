<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model
{
	use SoftDeletes;

  protected $fillable = [
		'name', 'type_id', 'duration', 'minimum', 'currency_id', 'expired_date', 'description'
	];

	public function type()
	{
		return $this->hasOne('App\Type', 'id', 'type_id');
	}

	public function currency()
	{
		return $this->hasOne('App\Currency', 'id', 'currency_id');
	}

	public function itinerary()
	{
		return $this->hasMany('App\Itinerary', 'package_id', 'id');
	}
}
