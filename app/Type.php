<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Type extends Model
{
		use SoftDeletes;

		protected $fillable = [
			'name'
		];

		public function package()
		{
			return $this->belongsTo('App\Package', 'type_id', 'id');
		}
}
