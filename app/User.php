<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'email_verified_at','password', 'country_id', 'date_of_birth', 'gender', 'phone', 'address', 'photo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
		];
		
		public function country()
		{
			return $this->hasOne('App\Country', 'id', 'country_id');
		}

		public function jenis_kelamin($jk)
		{
			if ($jk == 1) {
				$jk = 'Laki-Laki';
			} else if ($jk == 2) {
				$jk = 'Perempuan';
			} else if ($jk == 3) {
				$jk = 'Lainnya';
			}
			return $jk;
		}
}
