<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = [
        'code', 'name'
		];
		
		public function user()
		{
			return $this->belongsTo('App\User', 'country_id', 'id');
		}
}
