<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Currency extends Model
{
		use SoftDeletes;

		protected $fillable = [
			'code', 'name', 'description'
		];

		public function package()
		{
			return $this->belongsTo('App\Package', 'currency_id', 'id');
		}
}
